﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace AMONIC_Airlines
{
    public partial class WindowChangeRole : Form
    {
        SqlDataAdapter adapter;
        DataSet dataset;
        string connection = "Data Source = localhost;Initial Catalog = Session1_74; Integrated Security = true"; // строка подключения
        public WindowChangeRole()
        {
            InitializeComponent();
            using (SqlConnection connect = new SqlConnection(connection))
            {
                adapter = new SqlDataAdapter("Select Email from Users", connect);
                dataset = new DataSet();
                adapter.Fill(dataset);
                for (int i = 0; i < dataset.Tables[0].Rows.Count; i++)
                {
                    comboBox1.Items.Add(dataset.Tables[0].Rows[i][0].ToString());
                }


            }
        }
        // закрытие формы
        private void WindowChangeRole_FormClosed(object sender, FormClosedEventArgs e)
        {
            this.Hide();
        }
        // закрытие формы
        private void button1_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            using (SqlConnection connect = new SqlConnection(connection))
            {
                adapter = new SqlDataAdapter("Update Users set Role", connect);
                dataset = new DataSet();
                adapter.Fill(dataset);
                for (int i = 0; i < dataset.Tables[0].Rows.Count; i++)
                {
                    comboBox1.Items.Add(dataset.Tables[0].Rows[i][0].ToString());
                }


            }
        }
    }
}
