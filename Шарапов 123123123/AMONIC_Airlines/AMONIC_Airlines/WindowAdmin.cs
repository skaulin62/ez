﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace AMONIC_Airlines
{
    public partial class WindowAdmin : Form
    {
        WindowLogin wL; // переменная класса формы входа
        WindowAddUser wAddU; // переменная класса формы вдобавлния пользхователя
        SqlDataAdapter adapter;
        DataSet dataset;
        string connection = "Data Source = localhost;Initial Catalog = Session1_74; Integrated Security = true"; // строка подключения
        public WindowAdmin(string role,string name)
        {
            InitializeComponent();
        }

        private void WindowAdmin_Load(object sender, EventArgs e)
        {

            // заполнение datagridview
            using(SqlConnection connect = new SqlConnection(connection))
            {
                adapter = new SqlDataAdapter("Select* from pred_2", connect);
                dataset = new DataSet();
                adapter.Fill(dataset);

                dataGridView1.DataSource = dataset.Tables[0];
            }
            // заполнение comboBox оффисами
            using (SqlConnection connect = new SqlConnection(connection))
            {
                adapter = new SqlDataAdapter("Select Title from Offices", connect);
                dataset = new DataSet();
                adapter.Fill(dataset);
                for(int i = 0; i < dataset.Tables[0].Rows.Count;i++)
                {
                    comboBox1.Items.Add(dataset.Tables[0].Rows[i][0].ToString());
                }
                comboBox1.Items.Add("All Offices");
               
            }
        }
        // Переадресация к форме входа
        private void exitToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            wL = new WindowLogin();
            this.Hide();
            wL.Show();
            
        }
        // Переадресация к форме входа
        private void WindowAdmin_FormClosed(object sender, FormClosedEventArgs e)
        {
            wL = new WindowLogin();
            this.Hide();
            wL.Show();
        }

        private void comboBox1_TabIndexChanged(object sender, EventArgs e)
        {
           
               
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            string comboOffice = comboBox1.Text;

            if (comboBox1.Text == "All Offices")
            {
                using (SqlConnection connect = new SqlConnection(connection))
                {
                    adapter = new SqlDataAdapter("Select * from pred_2", connect);
                    dataset = new DataSet();
                    adapter.Fill(dataset);
                    dataGridView1.DataSource = dataset.Tables[0];

                }
            }
            else
            {
                using (SqlConnection connect = new SqlConnection(connection))
                {
                    adapter = new SqlDataAdapter($"Select * from pred_2 Where Office like '{comboOffice}'", connect);
                    dataset = new DataSet();
                    adapter.Fill(dataset);
                    dataGridView1.DataSource = dataset.Tables[0];

                }
            }
        }
        // откр  формы добавл польз
        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            wAddU = new WindowAddUser();
            wAddU.Show();
        }

        private void button1_Click(object sender, EventArgs e)
        {

        }
    }
}
