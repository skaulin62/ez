﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AMONIC_Airlines
{
    public partial class WindowUser : Form
    {
        WindowLogin wL; // переменная класса форма входа
        public WindowUser(string role, string Name,string Surname, string email)
        {
            InitializeComponent();
            label1.Text = $"Hi {Name} {Surname}, Welcome to AMONIC Airlines";
        }
        // ВЫход
        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            wL = new WindowLogin();
            this.Hide();
            wL.Show();
        }
        // ВЫход
        private void WindowUser_FormClosed(object sender, FormClosedEventArgs e)
        {
            wL = new WindowLogin();
            this.Hide();
            wL.Show();
        }
    }
}
