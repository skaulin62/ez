﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace AMONIC_Airlines
{
    public partial class WindowAddUser : Form
    {
        
        SqlDataAdapter adapter;
        DataSet dataset;
        string connection = "Data Source = localhost;Initial Catalog = Session1_74; Integrated Security = true"; // строка подключения

        string roleid, officeid;
        public WindowAddUser()
        {
            InitializeComponent();
            // вставка оффисов в combobox
            using (SqlConnection connect = new SqlConnection(connection))
            {
                adapter = new SqlDataAdapter("Select Title from Offices", connect);
                dataset = new DataSet();
                adapter.Fill(dataset);
                for (int i = 0; i < dataset.Tables[0].Rows.Count; i++)
                {
                    comboBox1.Items.Add(dataset.Tables[0].Rows[i][0].ToString());
                }
             

            }
        }

        private void WindowAddUser_FormClosed(object sender, FormClosedEventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            if(comboBox1.Text == "" || comboBox2.Text == "" || textBox1.Text == "" || textBox2.Text == "" || textBox3.Text == "" || textBox4.Text == "")
            {
                MessageBox.Show("Заполните все поля!","Сообщение");
            } else
            {
                if (comboBox2.Text == "Admin")
                {
                    roleid = "1";

                }
                else
                {
                    roleid = "0";
                }
                using (SqlConnection connect = new SqlConnection(connection))
                {
                    adapter = new SqlDataAdapter($"Select ID from Offices Where Title like '{comboBox1.Text}'", connect);
                    dataset = new DataSet();
                    adapter.Fill(dataset);

                    officeid = dataset.Tables[0].Rows[0][0].ToString();

                }
                // добавление данных в табл users
                using (SqlConnection connect = new SqlConnection(connection))
                {
                    SqlCommand comm = new SqlCommand("insert into Users(RoleID, Email,Password, FirstName, LastName, OfficeID,BirthDate,Active) values(@role,@email,@pass,@name,@name2,@office,@date,@active)", connect);

                    comm.Parameters.AddWithValue("@role", roleid);
                    comm.Parameters.AddWithValue("@email", textBox1.Text);
                    comm.Parameters.AddWithValue("@pass", textBox2.Text);
                    comm.Parameters.AddWithValue("@name", textBox3.Text);
                    comm.Parameters.AddWithValue("@name2", textBox4.Text);
                    comm.Parameters.AddWithValue("@office", officeid);
                    comm.Parameters.AddWithValue("@date", maskedTextBox1.Text);
                    comm.Parameters.AddWithValue("@active", "True");
                    connect.Open();
                    try
                    {
                        comm.ExecuteNonQuery();
                    } catch (Exception exp)
                    {
                        MessageBox.Show(exp.ToString(), "Сообщение");
                    }
                }
                MessageBox.Show("Пользователь добавлен!","Сообщение");
            }
            
        }
    }
}
