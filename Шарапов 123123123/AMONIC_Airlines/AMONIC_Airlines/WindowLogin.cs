﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace AMONIC_Airlines
{
    public partial class WindowLogin : Form
    {
        SqlDataAdapter adapter;
        DataSet dataset;
        string connection = "Data Source = localhost;Initial Catalog = Session1_74; Integrated Security = true"; // строка подключения

        WindowAdmin wA; // переменная класса формы админа
        WindowUser wU; // переменная класса формы пользователя
        public WindowLogin()
        {
            InitializeComponent();
        }

        // Выход из программы
        private void button2_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
        int count_try_login = 0;

        // КОд проверки пользователя в базе данных
        private void button1_Click(object sender, EventArgs e)
        {
            string role = null, Name = null, Surname = null, email = null;
            bool open = false;
            bool Active = true;
            using(SqlConnection connect = new SqlConnection(connection))
            {
                adapter = new SqlDataAdapter("Select* from pred_1", connect);
                dataset = new DataSet();
                adapter.Fill(dataset);
                for(int i = 0;i < dataset.Tables[0].Rows.Count; i++)
                {
                    if(textBox1.Text == dataset.Tables[0].Rows[i][1].ToString())
                    {
                        if(textBox2.Text == dataset.Tables[0].Rows[i][2].ToString())
                        {
                            open = true;
                            role = dataset.Tables[0].Rows[i][0].ToString();
                            Name = dataset.Tables[0].Rows[i][3].ToString();
                            Surname = dataset.Tables[0].Rows[i][4].ToString();
                            email = dataset.Tables[0].Rows[i][1].ToString();
                            Active = Convert.ToBoolean(dataset.Tables[0].Rows[i][6]);
                        }
                    }
                }
                if(open && Active)
                {
                   
                        if (role == "User")
                        {
                            wU = new WindowUser(role, Name, Surname, email);
                            this.Hide();
                            wU.Show();
                        }
                        if (role == "Administrator")
                        {
                            wA = new WindowAdmin(role, Name);
                            this.Hide();
                            wA.Show();
                        }
                    }
                  
                else
                {
                    if (Active)
                    {
                        count_try_login++;
                        if (count_try_login >= 3 && tickTimer == 0)
                        {

                            timer1.Start();
                        }
                        if (tickTimer > 0)
                        {
                            MessageBox.Show($"Повторите попытку через {10 - tickTimer}");
                        }
                        if (count_try_login < 3)
                        {
                            MessageBox.Show("Неверный логин или пароль!", "Сообщение");
                        }

                          
                    } else
                    {
                        MessageBox.Show("Доступ запрещен!", "Сообщение");
                    }
                }
            }
        }
        int tickTimer = 0; // счетчик
        private void timer1_Tick(object sender, EventArgs e)
        {
            tickTimer++;
            if(tickTimer == 10)
            {
                tickTimer = 0;
                timer1.Stop();
                count_try_login = 0;
            }
        }
        // Выход из программы
        private void WindowLogin_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }
    }
}
